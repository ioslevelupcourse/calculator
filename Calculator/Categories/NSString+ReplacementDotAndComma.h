//
//  NSString+ReplacementDotAndComma.h
//  Calculator
//
//  Created by Admin on 29.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ReplacementDotAndComma)

- (NSString *)commaToDot;

- (NSString *)dotToComma;

@end
