//
//  NSString+ReplacementDotAndComma.m
//  Calculator
//
//  Created by Admin on 29.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "NSString+ReplacementDotAndComma.h"

#define LUCommaString   @","
#define LUDotString     @"."

@implementation NSString (ReplacementDotAndComma)

- (NSString *)commaToDot {

     return [self stringByReplacingOccurrencesOfString:LUCommaString
                                            withString:LUDotString];
}

- (NSString *)dotToComma{
    
    return [self stringByReplacingOccurrencesOfString:LUDotString
                                           withString:LUCommaString];
}

@end
