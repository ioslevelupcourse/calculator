//
//  State.m
//  Calculator
//
//  Created by Admin on 03.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "State.h"

typedef NS_ENUM(NSInteger, Inputed) {
    InputedDigital,
    InputedOperation,
    IputedNot
};

@interface State ()

@property (nonatomic, strong) NSArray *binaryOperations;

@property (nonatomic, assign) BOOL isSeveralOperation; //ModeInputMultiEqual

@property (nonatomic, assign) Operation operationLastValue; //Reset varible, ModeInputNormal

@property (nonatomic, assign) Inputed inputed; //countMultiInput -> ModeInputMultiResult
@property (nonatomic, assign) Inputed inputedLastValue; //countMultiInput -> ModeInputMultiResult
@property (nonatomic, assign) NSInteger countMultiInput; //ModeInputMultiResult

@end

@implementation State
@synthesize result = _result;

- (instancetype)init {
    self = [super init];
    if (self) {
    _result = ModeInputNormal;
    self.inputed = IputedNot;
    self.binaryOperations = @[@(OperationSeparate),
                              @(OperationMultiply),
                              @(OperationMinus),
                              @(OperationPlus),
                              @(OperationEqually)];
    self.operationLastValue = OperetionNotFound;
    self.countMultiInput = 0;
    self.isSeveralOperation = NO;
        
    }
    return self;
}

#pragma mark - Setters Getters Public variable
- (void)setInputDigital:(BOOL)inputDigital {
    [self modeInputDigital:inputDigital inputOperation:self.inputOperation];
    _inputDigital = NO;
    if (self.result == ModeInputMultiEqual) {
        self.result = ModeInputNormal;
    }
}

- (void)setInputOperation:(BOOL)inputOperation {
    [self modeInputDigital:self.inputDigital inputOperation:inputOperation];
    _inputOperation = NO;
}

- (void)setResult:(ModeInput)result {
    [self printEnumModeInput:_result message:@"BEFOR"];
    //set first time - ModeInputStartMultiEqual, second time - ModeInputMultiEqual
    static BOOL isStartMultiEqual = NO;
    if (result == ModeInputMultiEqual && !isStartMultiEqual) {
        _result = ModeInputStartMultiEqual;
        isStartMultiEqual = YES;
        [self printEnumModeInput:_result message:@"AFTER"];
        return;
    } else if (result == ModeInputStartMultiEqual) {
        _result = ModeInputMultiEqual;
        [self printEnumModeInput:_result message:@"AFTER"];
        return;
    }
    if (result != ModeInputStartMultiEqual &&
        result != ModeInputMultiEqual) {
        isStartMultiEqual = NO;
    }
    _result = result;
    [self printEnumModeInput:_result message:@"AFTER"];
}

- (ModeInput)getResult {
    return _result;
}

#pragma mark - Setters Private variable
- (void)setInputed:(Inputed)inputed {
    _inputedLastValue = _inputed; //Save current to last
    _inputed = inputed;
}

#pragma mark - Privat
- (void)modeInputDigital:(BOOL)inputDigital inputOperation:(BOOL)inputOperation {
    NSInteger static countInpOper = 0;
    //Save input current state
    if (inputDigital) {
        self.inputed = InputedDigital;
        self.isSeveralOperation = NO;
        countInpOper = 0;
        
        if (self.operationLastValue == OperationEqually) {
            [self resetAll];
        }
        
    } else if (inputOperation) {
        self.inputed = InputedOperation;
        
    }
    //ModeInputMultiEqual
    if (inputOperation) {
        countInpOper += 1;
    }
    if (countInpOper >= 2) {
        self.isSeveralOperation = YES;
    }
}

- (void)currentSelfOperatution:(Operation)selfOperation operation:(Operation)operation {
    if (operation == OperationReset) {
        [self resetAll];
        return;
    }
    //Input Error, Change operation of calculator.operation
    if (self.inputedLastValue == InputedOperation &&
        [self recognizesTypeOperation:self.operationLastValue] == OperationTypeBinary &&
        operation != OperationEqually &&
        self.operationLastValue != OperationEqually) {
        self.result = ModeInputNotReal;
        return;
    }

    //ModeInputMultiEqual
    if (self.inputedLastValue == InputedDigital &&
        self.inputed == InputedOperation &&
        operation != OperationEqually) {
        self.countMultiInput += 1;
    }
    //New calculate
    if (self.inputed == InputedDigital &&
        self.operationLastValue == OperationEqually) {
        self.countMultiInput = 0;
        self.isSeveralOperation = NO;
    }
    //Result calculate
    if (self.isSeveralOperation && operation == OperationEqually) {
        self.result = ModeInputMultiEqual;
    } else if (self.countMultiInput >= 3) {
        self.result = ModeInputMultiResult;
    } else if ([self recognizesTypeOperation:self.operationLastValue] == OperationTypeBinary &&
        operation == OperationEqually) {
//        self.countMultiInput = 0;
        self.result = ModeInputNormal;
    }
    
    self.operationLastValue = operation;
}

- (void)resetAll {
    self.result = ModeInputNormal;
    self.countMultiInput = NO;
    self.inputed = IputedNot;
    self.inputedLastValue = IputedNot;
    self.operationLastValue = OperationReset;
    self.isSeveralOperation = NO;
}

- (OperationType)recognizesTypeOperation:(Operation)operation {
    NSUInteger index = [self.binaryOperations indexOfObject:@(operation)];
    if (index == NSNotFound) {
        return OperationTypeUnary;
    } else {
        return OperationTypeBinary;
    }
}

- (void)printEnumModeInput:(ModeInput)operation message:(NSString *)message {
#define EP(x) [x] = #x
    const char* strings[] = {EP(ModeInputNormal),
        EP(ModeInputMultiResult),
        EP(ModeInputStartMultiEqual),
        EP(ModeInputMultiEqual),
        EP(ModeInputNotReal)};
    
    NSLog(@"%@ %s",message, strings[operation]);
}


@end
