//
//  CalculatorElectronic.m
//  Calculator
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "CalculatorElectronic.h"

@interface CalculatorElectronic ()

@end

@interface CalculatorElectronic ()

@property (nonatomic, strong, nullable) NSNumber *accumulator;
@property (nonatomic, strong, nullable) NSNumber *saveAccumulator;

@property (nonatomic, assign) Operation operation;
@property (nonatomic, assign) Operation saveOperation;

@end

@implementation CalculatorElectronic

#pragma mark - Constant
double const LUZero = 0.f;

#pragma mark - Initialize
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.state = [State new];
        self.accumulator = nil; //self.operation = OperetionNotFound;
        self.saveAccumulator = nil;
        self.saveOperation = OperetionNotFound;
    }
    return self;
}

#pragma mark - Setters Privat
- (void)setResult:(double)result {
    _result = result;
    self.accumulator = nil;
}

- (void)setAccumulator:(NSNumber *)accumulator {
    _accumulator = accumulator;
    if (accumulator == nil) {
        _isWait = NO;
        self.operation = OperetionNotFound;
    } else {
        _isWait = YES;
    }
}

- (BOOL)useNoirmalStateOperation:(Operation)operation {
        NSArray *operations = @[@(OperationSeparate),
                                         @(OperationMultiply),
                                         @(OperationMinus),
                                         @(OperationPlus)];
        NSUInteger index = [operations indexOfObject:@(operation)];
        if (index == NSNotFound) {
            return NO;
        } else {
            return YES;
        }
}

#pragma mark - Operation Functions
- (void)takeOnCalculationOperation:(Operation)operation operand:(double)operand {
    //Print
    NSLog(@"------");
    [self printEnumOperation:operation message:@"OPERATION"];
    [self printEnumOperation:self.operation message:@"SELF.OPERATION"];
    NSLog(@"OPERAND %f", operand);
    
    //Definition of the current mode of calculation
    [self.state currentSelfOperatution:self.operation operation:operation];
    
    if (self.state.result == ModeInputStartMultiEqual) {
        self.saveAccumulator = self.accumulator;
        self.saveOperation = self.operation;
        self.operation = operation;
    } else if (self.state.result == ModeInputMultiEqual) {
        self.operation = operation;
    } else {
        self.saveAccumulator = nil;
        self.saveOperation = OperetionNotFound;
    }
    //В случае неправильного ввода операции.
    if (self.state.result == ModeInputNotReal) {
        self.operation = operation;
        self.accumulator = @(operand);
    }
    if ([self.state recognizesTypeOperation:operation] == OperationTypeUnary) {
        [self unarOperation:operation operand:operand];
        return;
    }
    
    [self executionOperation:operation operand:operand];
}

- (void)executionOperation:(Operation)operation operand:(double)operand {
    switch (self.operation) {
        case OperationPlus: {//binary operation
            if (self.state.result == ModeInputNormal) {
                NSLog(@"case OperationPlus, self.state.result == ModeInputNormal");
                
                self.result = self.accumulator.doubleValue + operand;
                
            } else if (self.state.result == ModeInputMultiResult) {
                NSLog(@"case OperationPlus, self.state.result == ModeInputMultiResult");
                
                self.result = self.result + operand;
                
            } else if (self.state.result == ModeInputStartMultiEqual ||
                       self.state.result == ModeInputMultiEqual) {
                NSLog(@"case OperationPlus, self.state.result == ModeInputMultiEqual");
                
                self.result = self.accumulator.doubleValue + operand;
                
            } else if (self.state.result == ModeInputNotReal) {
                NSLog(@"case OperationPlus, self.state.result == ModeInputNotReal");
            }
        }
            break;
            
        case OperationMinus:
            if (self.accumulator) {
                self.result = self.accumulator.doubleValue - operand;
            } else {
                self.accumulator = @(operand);
            }
            break;
            
        case OperationMultiply:
            if (self.accumulator) {
                self.result = self.accumulator.doubleValue * operand;
            } else {
                self.accumulator = @(operand);
            }
            break;
            
        case OperationSeparate:
            if (self.accumulator) {
                self.result = self.accumulator.doubleValue / operand;
            } else {
                self.accumulator = @(operand);
            }
            break;
            
        case OperationEqually://func operation
            NSLog(@"Equaly");
            if (self.state.result == ModeInputStartMultiEqual ||
                self.state.result == ModeInputMultiEqual) {
                    self.accumulator = self.saveAccumulator;
                    self.operation = self.saveOperation;
                    [self executionOperation:self.saveOperation operand:operand];
            }

            break;
            
        case OperetionNotFound: {
            self.operation = operation;
            self.accumulator = @(operand);
        }
            break;
            
        default:
            NSLog(@"BinaryDefaultInSwitchOperationRate");
            break;
    }
    
    NSLog(@"RESULT %f", self.result);
}

- (void)unarOperation:(Operation)operation operand:(double)operand {
    switch (operation) {
            
        case OperationReset:
            NSLog(@"Reset");
            self.result = LUZero;
            break;
            
            //unar operation
        case OperationPlusMinus:
            self.result = -operand;
            break;
            
        case OperationPercent:
            self.result = operand/100;
            break;
            
        default:
            NSLog(@"UnarDefaultInSwitchOperationRate");
            break;
    }
    
}

- (void)printEnumOperation:(Operation)operation message:(NSString *)message {
#define EP(x) [x] = #x
    const char* strings[] = {EP(OperationReset),
        EP(OperationPlusMinus),
        EP(OperationPercent),
        EP(OperationSeparate),
        EP(OperationMultiply),
        EP(OperationMinus),
        EP(OperationPlus),
        EP(OperationEqually),
        EP(OperetionNotFound)
    };
    
    NSLog(@"%@ %s",message, strings[operation]);
}

@end
