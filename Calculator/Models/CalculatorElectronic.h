//
//  CalculatorElectronic.h
//  Calculator
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "State.h"

#import "Defines.h"

@interface CalculatorElectronic : NSObject

@property (nonatomic, strong) State *state;

//Result and current status calculation
@property (nonatomic, assign, readonly) double result;
@property (nonatomic, assign, readonly) BOOL isWait;

////always keeps NO, works as action (Electronic necessary feedback input)
//@property (nonatomic, assign) BOOL inputDigital;
//@property (nonatomic, assign) BOOL inputOperation;

- (void)takeOnCalculationOperation:(Operation)operation operand:(double)operand;

@end
