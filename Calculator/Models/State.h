//
//  State.h
//  Calculator
//
//  Created by Admin on 03.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Defines.h"

typedef NS_ENUM(NSInteger, ModeInput) {
    ModeInputNormal = 1,
    ModeInputMultiResult,
    ModeInputStartMultiEqual,
    ModeInputMultiEqual,
    ModeInputNotReal
};

@interface State : NSObject

@property (nonatomic, assign) ModeInput result;

//always keeps NO, works as action (Electronic necessary feedback input)
@property (nonatomic, assign) BOOL inputDigital;
@property (nonatomic, assign) BOOL inputOperation;

- (void)currentSelfOperatution:(Operation)selfOperation operation:(Operation)operation;

- (OperationType)recognizesTypeOperation:(Operation)operation;

@end
