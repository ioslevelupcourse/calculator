//
//  Defines.h
//  Calculator
//
//  Created by Admin on 05.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#ifndef Defines_h
#define Defines_h
#endif /* Defines_h */

typedef NS_ENUM(NSInteger, Operation) {
    OperationReset        = 101, //"AC"
    OperationPlusMinus    = 102, //"±"
    OperationPercent      = 103, //"%"
    OperationSeparate     = 104, //"÷"
    OperationMultiply     = 105, //"×"
    OperationMinus        = 106, //"−"
    OperationPlus         = 107, //"+"
    OperationEqually      = 108, //"="
    OperetionNotFound     = 999  //NotFound
};

typedef NS_ENUM(NSInteger, OperationType) {
    OperationTypeUnary,
    OperationTypeBinary,
};

//#define buttonDigit0 0
//#define buttonDigit1 1
//#define buttonDigit2 2
//#define buttonDigit3 3
//#define buttonDigit4 4
//#define buttonDigit5 5
//#define buttonDigit6 6
//#define buttonDigit7 7
//#define buttonDigit8 8
//#define buttonDigit9 9

//#define LUBButtonOperationComma        100

//#define buttonOperationReset        101

//#define buttonOperationPlusMinus    102 //"±"
//#define buttonOperationPercent      103 //"%"
//#define buttonOperationSeparate     104 //"÷"
//#define buttonOperationMultiply     105 //"×"
//#define buttonOperationMinus        106 //"−"
//#define buttonOperationPlus         107 //"+"
//#define buttonOperationEqually      108 //"="

//#define buttonOperationPlus     @"+"

//typedef enum {
//    OperationReset,
//    OperationPlusMinus,
//    OperationPercent,
//    OperationPlus,
//    OperationMinus,
//    OperationMultiply,
//    OperationSeparate,
//    OperationEqually
//} Operationr;


//NSDictionary *journal =
//[NSDictionary dictionaryWithObjectsAndKeys:
// buttonOperationPlus, operationPlus ,nil];

//NSDictionary *stateStrings = @{
//  .operationPlus : buttonOperationPlus,
//  nil
//  };

//NSString *stateString = [stateStrings objectForKey:@(state)];

/*
 %
 "π"
 "e"
 "±"
 "√"
 "cos"
 "×"
 "÷"
 "+"
 "−"
 "="
 */


/*    switch (operation) {
 case OperationPlusMinus:
 
 [self setResult:-operand];
 
 break;
 
 case OperationPercent:
 
 [self setResult:operand/100];
 
 break;
 
 case OperationPlus:
 
 if (self.accumulator) {
 [self setResult:(operand + self.accumulator.doubleValue)];
 } else {
 [self setAccumulator:@(operand)];
 }
 
 break;
 
 case OperationMinus:
 
 if (self.accumulator) {
 [self setResult:(operand - self.accumulator.doubleValue)];
 } else {
 [self setAccumulator:@(operand)];
 }
 
 break;
 
 case OperationMultiply:
 
 if (self.accumulator) {
 [self setResult:(operand * self.accumulator.doubleValue)];
 } else {
 [self setAccumulator:@(operand)];
 }
 
 break;
 
 case OperationSeparate:
 
 if (self.accumulator) {
 [self setResult:(operand / self.accumulator.doubleValue)];
 } else {
 [self setAccumulator:@(operand)];
 }
 
 break;
 
 case OperationEqually:
 
 NSLog(@"Equaly");
 
 break;
 
 case OperationReset:
 
 NSLog(@"Reset");
 [self setAccumulator:nil];
 
 break;
 
 
 default:
 NSLog(@"defaultInSwitchOperationRate");
 break;
 }*/
/*
 //- (void)printEnumModeInput:(ModeInput)operation message:(NSString *)message {
 //#define EP(x) [x] = #x
 //    const char* strings[] = {EP(ModeInputNormal),
 //        EP(ModeInputMultiEqual),
 //        EP(ModeInputMultiEqual)};
 //
 //    NSLog(@"%@ %s",message, strings[operation]);
 //}
 
 
 //- (void)binaryOperationOperand:(double)operand {
 
 //    if (self.operation != OperetionNotFound && !self.accumulator) {
 //        self.accumulator = @(operand);
 //    }
 
 
 //}
 
 //- (void) unaryOperation:(double)operand {
 //
 //    switch (self.operation) {
 //
 //     }
 //
 //}
 
 //- (void)setOperation:(Operation)operation {
 //    self.operation = operation;
 //}
 //
 //- (void)setOperand:(double)value {
 //    self.accumulator = value;
 //}
 
 //- (double) operationPlus: (double) operand1: (double) operand2 {
 //  return (operand1 + operand2);
 //}
 //
 //- (double) operationMinus: (double) operand1: (double) operand2 {
 //    return (operand1 - operand2);
 //}*/
 
 
 /*
 OperationPlusMinus    102 //"±"
 OperationPercent      103 //"%"
 OperationSeparate     104 //"÷"
 OperationMultiply     105 //"×"
 OperationMinus        106 //"−"
 OperationPlus         107 //"+"
 OperationEqually      108 //"="
 */

/*
 //#pragma mark - Setters Public
 //- (void)setInputDigital:(BOOL)inputDigital {
 //    [self modeInputDigital:inputDigital inputOperation:self.inputOperation];
 //    _inputDigital = NO;
 //}
 //- (void)setInputOperation:(BOOL)inputOperation {
 //    [self modeInputDigital:self.inputDigital inputOperation:inputOperation];
 //    _inputOperation = NO;
 //}
 //
 //#pragma mark - Privat
 //- (void)modeInputDigital:(BOOL)inputDigital inputOperation:(BOOL)inputOperation {
 //        NSInteger static countInputOperation = 0;
 //
 //
 //    if (inputOperation) {
 //            countInputOperation += 1;
 //        }
 //        if (countInputOperation >= 2) {
 //            self.modeInput = ModeInputMultiEqual;
 //        }
 //
 //        if (inputDigital) {
 //            self.modeInput = ModeInputNormal;
 //            countInputOperation = 0;
 //        } else if (inputOperation) {
 //            self.modeInput = countInputOperation;
 //        }
 //}
 */
