//
//  AppDelegate.h
//  Calculator
//
//  Created by Admin on 05.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

