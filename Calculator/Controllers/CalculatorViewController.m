//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Admin on 05.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "CalculatorViewController.h"

typedef NS_ENUM(NSInteger, DisplayMode) {
    DisplayModeInput,
    DisplayModeIndicate,
};

@interface CalculatorViewController ()

@property (nonatomic, weak) IBOutlet UILabel *displayLabel;

@property (nonatomic, assign) NSInteger maxDigitInDisplay;
@property (nonatomic, assign) BOOL isUsedComma;
@property (nonatomic, strong) CalculatorElectronic *electronic;
@property(nonatomic, assign, getter=getDisplayValue) double displayValue;
@property(nonatomic, assign) DisplayMode displayMode;

@end

@implementation CalculatorViewController
@synthesize isUsedComma = _isUsedComma;
@synthesize displayValue = _displayValue;

#pragma mark - Constsants
NSInteger const LUButtonZeroTag     = 0;
NSInteger const LUButtonCommaTag    = 100;
NSString *const LUCommaString       = @",";
NSString *const LUZeroDigitString   = @"0";

#pragma mark - Initialize

- (void)initialize {
    CalculatorElectronic * electronic = [[CalculatorElectronic alloc] init];
    self.electronic = electronic;
    self.isUsedComma = NO;
    self.displayMode = DisplayModeInput;
    self.maxDigitInDisplay = 11;
}

- (void)setDisplayValue:(double)displayValue {
    _displayValue = displayValue;
}

- (double)getDisplayValue {
    return _displayValue;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

#pragma mark - Action
- (IBAction)touchDigitButton:(UIButton *)sender {
    self.electronic.state.inputDigital = YES;
    NSString *displayText = self.displayLabel.text;
    if (displayText.length >= (self.maxDigitInDisplay - 1)) {
        return;
    }
    if ([displayText isEqualToString: LUZeroDigitString] &&
        sender.tag == LUButtonZeroTag) {
        return;
    }
    if (self.isUsedComma && sender.tag == LUButtonCommaTag) {
        return;
    }
    if ([displayText isEqualToString:LUZeroDigitString] &&
        sender.tag == LUButtonCommaTag) {
        self.displayLabel.text = [LUZeroDigitString stringByAppendingString:LUCommaString];
        self.isUsedComma = YES;
        return;
    }
    if (self.displayMode == DisplayModeIndicate) {
        displayText = @"";
        self.displayMode = DisplayModeInput;
    } else if ([displayText isEqualToString:LUZeroDigitString] &&
               sender.tag != LUButtonCommaTag) {
        displayText = @"";
    }
    self.displayLabel.text = [NSString stringWithFormat:@"%@%@", displayText,
     sender.tag == LUButtonCommaTag ? LUCommaString : @(sender.tag).stringValue];
    if (sender.tag == LUButtonCommaTag) {
        self.isUsedComma = YES;
    }
}

- (IBAction)touchOperationButton:(UIButton *)sender {
    self.electronic.state.inputOperation = YES;
    Operation operation = (Operation)sender.tag;
    double operand = [[self.displayLabel.text commaToDot] doubleValue];
    [self.electronic takeOnCalculationOperation:operation operand:operand];
    NSLog(@"VC is self.electronic.isWait:%d",self.electronic.isWait);
    if (!self.electronic.isWait) {
        self.displayLabel.text = [@(self.electronic.result).stringValue dotToComma];
        self.displayMode = DisplayModeIndicate;
    } else {
        self.displayMode = DisplayModeIndicate;
    }
    self.isUsedComma = NO;
}
@end
