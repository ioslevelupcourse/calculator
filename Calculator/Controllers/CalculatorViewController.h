//
//  CalculatorViewController.h
//  Calculator
//
//  Created by Admin on 05.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CalculatorElectronic.h"

#import "Defines.h"

#import "NSString+ReplacementDotAndComma.h"

@interface CalculatorViewController : UIViewController

@end
